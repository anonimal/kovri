/**                                                                                           //
 * Copyright (c) 2015-2019, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 */

#ifndef SRC_CLIENT_TUNNEL_DATA_H_
#define SRC_CLIENT_TUNNEL_DATA_H_

namespace kovri
{
namespace client
{
/// @class ACL
/// @brief Access Control List for tunnel attributes
struct ACL
{
  ACL() : is_white(false), is_black(false) {}
  std::string list;
  bool is_white, is_black;
};

// TODO(unassigned): signature type (see #369)
/// @class TunnelAttributes
/// @brief Attributes for client/server tunnel
/// @notes For details, see tunnels configuration key
struct TunnelAttributes
{
  TunnelAttributes() : port(0), dest_port(0), in_port(0) {}
  std::string name, type, dest, address, keys;
  std::uint16_t port, dest_port, in_port;
  ACL acl{};
};

/// @enum Key
/// @brief Tunnels config attribute key for const tunnel param string
enum struct Key : std::uint8_t
{
  /// @var Type
  /// @brief Key for type of tunnel  (client/server/HTTP, etc.)
  Type,
  /// @var Client
  /// @brief Key for client tunnel
  Client,
  /// @var IRC
  /// @brief Key for IRC tunnel
  IRC,
  /// @var Server
  /// @brief Key for server tunnel
  Server,
  /// @var HTTP
  /// @brief Key for HTTP tunnel
  HTTP,
  /// @var Address
  /// @brief Key for local listening address that you or service connects to
  /// @notes Should default to 127.0.0.1
  Address,
  /// @var Dest
  /// @brief Key for I2P hostname or .b32 address
  Dest,
  /// @var DestPort
  /// @brief Key for I2P destination port used in destination
  DestPort,
  /// @var InPort
  /// @brief Key for I2P service port. If unset, should be the same as 'port'
  InPort,
  /// @var Whitelist
  /// @brief Key for Access Control whitelist of I2P addresses for server tunnel
  Whitelist,
  /// @var Blackslist
  /// @brief Key for Access Control blacklist of I2P addresses for server tunnel
  Blacklist,
  /// @var Port
  /// @brief Key for port of our listening client or server tunnel
  ///   (example: port 80 if you are hosting website)
  Port,
  /// @var Keys
  /// @brief Key for client tunnel identity
  ///   or file with LeaseSet of local service I2P address
  Keys,
};

/// @brief Tunnel attribute accessor
/// @tparam t_attr String type (forward-compatible for string_view)
/// @param key Key enumerator
template <typename t_attr = std::string>
t_attr GetAttribute(Key key)
{
  switch (key)
    {
      // Section types
      case Key::Type:
        return "type";
        break;
      case Key::Client:
        return "client";
        break;
      case Key::IRC:
        return "irc";
        break;
      case Key::Server:
        return "server";
        break;
      case Key::HTTP:
        return "http";
        break;
      // Client-tunnel specific
      case Key::Dest:
        return "dest";
        break;
      case Key::DestPort:
        return "dest_port";
        break;
      // Server-tunnel specific
      case Key::InPort:
        return "in_port";
        break;
      case Key::Whitelist:
        return "white_list";
        break;
      case Key::Blacklist:
        return "black_list";
        break;
      // Tunnel-agnostic
      case Key::Address:
        return "address";
        break;
      case Key::Port:
        return "port";
        break;
      case Key::Keys:
        return "keys";
        break;
      default:
        return "";  // not needed (avoids nagging -Wreturn-type)
        break;
    };
}

}  // namespace client
}  // namespace kovri

#endif  // SRC_CLIENT_TUNNEL_DATA_H_
