/**                                                                                           //
 * Copyright (c) 2015-2018, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 */

#ifndef SRC_CLIENT_UTIL_CONFIG_H_
#define SRC_CLIENT_UTIL_CONFIG_H_

#include "core/util/config.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include "client/tunnel.h"

#include "core/util/exception.h"
#include "core/util/filesystem.h"

namespace kovri
{
namespace client
{
/// @class Configuration
/// @brief Client configuration implementation
/// @note Core configuration SHOULD be initialized first
class Configuration
{
 public:
  explicit Configuration(const core::Configuration& core_config);

  // TODO(anonimal): overload ctor
  ~Configuration();

  /// @brief Parses tunnel configuration file
  /// @warning Logging must be setup to see any debug output
  void ParseConfig();

  /// @brief Gets tunnels config member
  /// @return Reference to tunnels attributes vector member
  const std::vector<TunnelAttributes>& GetParsedTunnelsConfig() const noexcept
  {
    return m_TunnelsConfig;
  }

  /// @brief Gets complete path + name of tunnels config
  /// @return Boost filesystem path of file
  /// @warning Config file must first be parsed
  const boost::filesystem::path GetConfigPath() const
  {
    std::string tunnels_config =
        m_CoreConfig.GetMap()["tunnelsconf"].defaulted()
            ? "tunnels.conf"
            : m_CoreConfig.GetMap()["tunnelsconf"].as<std::string>();
    boost::filesystem::path file(tunnels_config);
    if (!file.is_complete())
      file = core::GetPath(core::Path::Config) / file;
    return file;
  }

  /// @brief Get core configuration object
  const core::Configuration& GetCoreConfig() const noexcept
  {
    return m_CoreConfig;
  }

 private:
  /// @brief Exception dispatcher
  static core::Exception m_Exception;

  /// @var m_TunnelsConfig
  /// @brief Vector of all sections in a tunnel configuration
  std::vector<TunnelAttributes> m_TunnelsConfig{};

  /// @brief Core configuration
  core::Configuration m_CoreConfig;
};

}  // namespace client
}  // namespace kovri

#endif  // SRC_CLIENT_UTIL_CONFIG_H_
