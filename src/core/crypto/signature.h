/**                                                                                           //
 * Copyright (c) 2013-2018, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 *                                                                                            //
 * Parts of the project are originally copyright (c) 2013-2015 The PurpleI2P Project          //
 */

#ifndef SRC_CORE_CRYPTO_SIGNATURE_H_
#define SRC_CORE_CRYPTO_SIGNATURE_H_

#include <cstdint>
#include <memory>

#include <cryptopp/naclite.h>

#include "core/crypto/signature_base.h"

namespace kovri {
namespace core {

/**
 *
 * DSA
 *
 */

namespace crypto
{
namespace PkLen
{
enum
{
  DSA = 128,
};
}  // namespace PkLen

namespace SkLen
{
enum
{
  DSA = 20,
};
}  // namespace SkLen

namespace SigLen
{
enum
{
  DSA = 40,
};
}  // namespace SigLen
}  // namespace crypto

/// @class DSAVerifier
class DSAVerifier : public Verifier {
 public:
  DSAVerifier(
      const std::uint8_t* signing_key);
  ~DSAVerifier();

  bool Verify(
      const std::uint8_t* buf,
      std::size_t len,
      const std::uint8_t* signature) const;

  std::size_t GetPublicKeyLen() const {
    return crypto::PkLen::DSA;
  }

  std::size_t GetSignatureLen() const {
    return crypto::SigLen::DSA;
  }

  std::size_t GetPrivateKeyLen() const {
    return crypto::SkLen::DSA;
  }

 private:
  class DSAVerifierImpl;
  std::unique_ptr<DSAVerifierImpl> m_DSAVerifierPimpl;
};

/**
 *
 * RSASHA5124096
 *
 */

namespace crypto
{
namespace PkLen
{
enum
{
  RSASHA5124096 = 512,
};
}  // namespace PkLen

namespace SkLen
{
enum
{
  RSASHA5124096 = 1024,
};
}  // namespace SkLen

namespace SigLen
{
enum
{
  RSASHA5124096 = 512,
};
}  // namespace SigLen
}  // namespace crypto

/**
 *
 * RSASHA5124096Raw
 *
 */

/// @class RSASHA5124096RawVerifier
class RSASHA5124096RawVerifier : public RawVerifier {
 public:
  explicit RSASHA5124096RawVerifier(
      const std::uint8_t* signing_key);
  ~RSASHA5124096RawVerifier();

  bool Verify(
      const std::uint8_t* signature);

  void Update(
      const std::uint8_t* signature,
      std::size_t len);

 private:
  class RSASHA5124096RawVerifierImpl;
  std::unique_ptr<RSASHA5124096RawVerifierImpl> m_RSASHA5124096RawVerifierPimpl;
};

/**
 *
 * Ed25519
 *
 */

// TODO(anonimal): continue with remaining crypto
namespace crypto
{
namespace PkLen
{
enum
{
  Ed25519 = CryptoPP::NaCl::crypto_sign_PUBLICKEYBYTES,
};
}  // namespace PkLen

namespace SkLen
{
enum
{
  Ed25519 = CryptoPP::NaCl::crypto_sign_SECRETKEYBYTES,
};
}  // namespace SkLen

namespace SigLen
{
enum
{
  Ed25519 = CryptoPP::NaCl::crypto_sign_BYTES,
};
}  // namespace SigLen
}  // namespace crypto

/// @class Ed25519Verifier
/// @brief Interface class for the EdDSA Ed25519 verifier
class Ed25519Verifier : public Verifier
{
 public:
  explicit Ed25519Verifier(const std::uint8_t* pk);

  ~Ed25519Verifier();

  /// @brief Verify signed message with given signature
  /// @param m Message (without signature)
  /// @param mlen Message length (without signature)
  /// @param sig Signature
  bool Verify(
      const std::uint8_t* m,
      const std::size_t mlen,
      const std::uint8_t* sig) const;

  std::size_t GetPublicKeyLen() const
  {
    return crypto::PkLen::Ed25519;
  }

  std::size_t GetPrivateKeyLen() const
  {
    return crypto::SkLen::Ed25519 - 32;  // An I2P'ism
  }

  std::size_t GetSignatureLen() const
  {
    return crypto::SigLen::Ed25519;
  }

 private:
  class Ed25519VerifierImpl;
  std::unique_ptr<Ed25519VerifierImpl> m_Ed25519VerifierPimpl;
};

/// @class Ed25519Signer
/// @brief Interface class for the EdDSA Ed25519 signer
class Ed25519Signer : public Signer
{
 public:
  /// @brief Create signer from a private key
  /// @param sk Private key
  /// @details The corresponding public key will be computed internally
  explicit Ed25519Signer(const std::uint8_t* sk);

  /// @brief Create signer from a keypair
  /// @param sk Private key
  /// @param pk Public key
  Ed25519Signer(const std::uint8_t* sk, const std::uint8_t* pk);

  ~Ed25519Signer();

  /// @brief Ed25519 sign a message
  /// @param m Message
  /// @param mlen Message size
  /// @param sig Output the signature (only) of the signed message
  void Sign(const std::uint8_t* m, const std::size_t mlen, std::uint8_t* sig)
      const;

 private:
  class Ed25519SignerImpl;
  std::unique_ptr<Ed25519SignerImpl> m_Ed25519SignerPimpl;
};

/// @brief Generate ed25519 keypair
void CreateEd25519KeyPair(std::uint8_t* sk, std::uint8_t* pk);

}  // namespace core
}  // namespace kovri

#endif  // SRC_CORE_CRYPTO_SIGNATURE_H_
