/**                                                                                           //
 * Copyright (c) 2015-2019, The Kovri I2P Router Project                                      //
 *                                                                                            //
 * All rights reserved.                                                                       //
 *                                                                                            //
 * Redistribution and use in source and binary forms, with or without modification, are       //
 * permitted provided that the following conditions are met:                                  //
 *                                                                                            //
 * 1. Redistributions of source code must retain the above copyright notice, this list of     //
 *    conditions and the following disclaimer.                                                //
 *                                                                                            //
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list     //
 *    of conditions and the following disclaimer in the documentation and/or other            //
 *    materials provided with the distribution.                                               //
 *                                                                                            //
 * 3. Neither the name of the copyright holder nor the names of its contributors may be       //
 *    used to endorse or promote products derived from this software without specific         //
 *    prior written permission.                                                               //
 *                                                                                            //
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY        //
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF    //
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL     //
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,       //
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,               //
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    //
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,          //
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF    //
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               //
 */

#include "tests/unit_tests/main.h"

#include "client/context.h"

struct Context
{
  Context()
      : keys(core::PrivateKeys::CreateRandomKeys()),
        // Destination derived from fixture keypair, should always produce same destination
        destination(
            std::make_shared<client::ClientDestination>(keys, false, nullptr))
  {
  }

  core::PrivateKeys keys;
  std::shared_ptr<client::ClientDestination> destination;
  client::LocalDestinations<client::ClientDestination> local_dests;
};

BOOST_FIXTURE_TEST_SUITE(ClientContext, Context);

BOOST_AUTO_TEST_CASE(Create)
{
  // Store and compare fixture destination
  auto dest = local_dests.Create(keys);
  BOOST_CHECK_EQUAL(dest->GetIdentHash(), destination->GetIdentHash());

  // Should have started automatically
  BOOST_CHECK_EQUAL(dest->IsRunning(), true);

  // If exists, should *not* return nullptr
  auto exists = local_dests.Create(keys);
  BOOST_REQUIRE_NE(exists, nullptr);
  BOOST_CHECK_EQUAL(exists->IsRunning(), true);
}

BOOST_AUTO_TEST_CASE(Find)
{
  // Not found because not stored
  auto dest = local_dests.Find(destination->GetIdentHash());
  BOOST_REQUIRE_EQUAL(dest, nullptr);

  // Create/store, then get hash
  dest = local_dests.Create(keys);
  const auto& ident = dest->GetIdentHash();

  // Should exist and equal fixture destination
  auto exists = local_dests.Find(ident);
  BOOST_REQUIRE_NE(exists, nullptr);
  BOOST_CHECK_EQUAL(exists->GetIdentHash(), ident);
  BOOST_CHECK_EQUAL(ident, destination->GetIdentHash());

  // Should not exist
  auto non_exists = std::make_shared<client::ClientDestination>(
      core::PrivateKeys::CreateRandomKeys(), false, nullptr);
  BOOST_REQUIRE_EQUAL(local_dests.Find(non_exists->GetIdentHash()), nullptr);
  // Uncommented when used in impl
  //BOOST_REQUIRE_EQUAL(local_dests.Find(non_exists), nullptr);
}

BOOST_AUTO_TEST_CASE(StartAndStop)
{
  BOOST_CHECK_EQUAL(destination->IsRunning(), false);
  BOOST_REQUIRE_NO_THROW(destination->Start());
  BOOST_CHECK_EQUAL(destination->IsRunning(), true);

  auto dest = local_dests.Create();
  BOOST_REQUIRE_NO_THROW(dest->Start());
  BOOST_CHECK_EQUAL(dest->IsRunning(), true);
  BOOST_REQUIRE_NO_THROW(dest->Stop());
  BOOST_CHECK_EQUAL(dest->IsRunning(), false);

  BOOST_REQUIRE_NO_THROW(local_dests.Start());
  BOOST_CHECK_EQUAL(dest->IsRunning(), true);
  BOOST_REQUIRE_NO_THROW(local_dests.Stop());
  BOOST_CHECK_EQUAL(dest->IsRunning(), false);
}

// Uncomment when used in impl
//BOOST_AUTO_TEST_CASE(Delete)
//{
//  auto dest = local_dests.Create();
//  BOOST_CHECK_EQUAL(local_dests.Delete(dest), true);
//
//  dest = local_dests.Create();
//  BOOST_CHECK_EQUAL(local_dests.Delete(dest->GetIdentHash()), true);
//}

BOOST_AUTO_TEST_SUITE_END()
