#!/usr/bin/env python

# Copyright (c) 2015-2018, The Kovri I2P Router Project
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are
# permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list
#    of conditions and the following disclaimer in the documentation and/or other
#    materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be
#    used to endorse or promote products derived from this software without specific
#    prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
# THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
# THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from unittest2 import TestCase

from nose2.tools import such

with such.A("client instance w/ install setup") as it:

    @it.has_test_setup
    def setup(case):
        "client instance test setup"
        root = "."
        args = "--log-level 5 --disable-ssu"

        # Ensure kovri data dir is available
        import subprocess

        p = subprocess.Popen(['make', 'install'], cwd=root)
        p.wait()

        import sys

        sys.path.insert(1, "./build")

        # Use built kovri python object
        import kovri_integration as kovri

        it.core = kovri.Core(args)
        it.client = kovri.Client(it.core)

    @it.has_test_teardown
    def teardown(case):
        "client instance test teardown"
        from os.path import expanduser
        from shutil import rmtree

        rmtree(expanduser('~') + "/.kovri")

    def instance_dirs_exist():
        from os.path import isdir, expanduser

        ret = False

        for _dir in ["client", "config", "core"]:
            ret = isdir(expanduser("~") + "/.kovri/" + _dir)
            if not ret: break

        return ret

    @it.should("init the client, start, reload, and stop")
    def test(case):
        try:
            # Initialize the client
            it.client.init()

            # Check that instance directories exist
            assert instance_dirs_exist()

            # Start the client
            it.client.start()

            # Check the client is running
            assert it.client.is_running() and it.core.is_running()

            # Reload the client
            it.client.reload()

            # Check the client is running
            assert it.client.is_running() and it.core.is_running()

            # Stop the client
            it.client.stop()

            # Check the client is no longer running
            assert not it.client.is_running() and not it.core.is_running()
        except Exception as e:
            print(str(e))
            assert False

    @it.should("throw when started without init")
    def test(case):
        try:
            # Start the client
            it.client.start()

            # Test should fail if exception is not raised
            assert False
        except Exception:
            # Expect an exception to be thrown
            assert True

it.createTests(globals())
